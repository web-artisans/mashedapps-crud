<?php

namespace Zagno\Crud\Response;

/**
 * @author Clive Zagno <clivez@gmail.com>
 */
class BadRequestResponse extends Response {

    /**
     * Resource BadRequest HTTP response
     *
     * @param String|Exception $message
     *
     * @return Response
     */
    public function __construct($message = 'Bad Request')
    {
        parent::__construct(['error' => $this->getMessage($message)], parent::HTTP_BAD_REQUEST);
    }
}
