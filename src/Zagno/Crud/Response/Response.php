<?php

namespace Zagno\Crud\Response;

use Illuminate\Http\Response as LaravelResponse;
use Illuminate\Support\Facades\Response as ResponseFacade;

/**
 * @author Clive Zagno <clivez@gmail.com>
 */
class Response extends LaravelResponse {

    protected function getMessage($message)
    {
        if(is_string($message)) {
            return $message;
        }

        return $message->getMessage();
    }
}
