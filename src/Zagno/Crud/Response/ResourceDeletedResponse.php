<?php

namespace Zagno\Crud\Response;

/**
 * @author Clive Zagno <clivez@gmail.com>
 */
class ResourceDeletedResponse extends Response {

    /**
     * Resource deleted HTTP response
     *
     * @param String|Exception $message
     *
     * @return Response
     */
    public function __construct($message = 'Resource Deleted')
    {
        parent::__construct(['success' => $this->getMessage($message)], parent::HTTP_OK);
    }
}
