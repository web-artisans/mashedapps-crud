<?php

namespace Zagno\Crud\Response;

/**
 * @author Clive Zagno <clivez@gmail.com>
 */
class ResourceNotFoundResponse extends Response {

    /**
     * Resource not found HTTP response
     *
     * @param String|Exception $message
     *
     * @return Response
     */
    public function __construct($message = 'Resource not found')
    {
        parent::__construct(['error' => $this->getMessage($message)], parent::HTTP_NOT_FOUND);
    }
}
