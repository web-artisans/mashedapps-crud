<?php

namespace Zagno\Crud\Response;

/**
 * @author Clive Zagno <clivez@gmail.com>
 */
class ResourceCreatedResponse extends Response {

    /**
     * Resource created HTTP response
     *
     * @param String|Exception $message
     *
     * @return Response
     */
    public function __construct($message = 'Resource Created', $data = array())
    {
        parent::__construct(
            ['success' => $this->getMessage($message)] + $data,
            parent::HTTP_CREATED
        );
    }
}

