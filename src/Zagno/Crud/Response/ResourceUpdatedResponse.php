<?php

namespace Zagno\Crud\Response;

/**
 * @author Clive Zagno <clivez@gmail.com>
 */
class ResourceUpdatedResponse extends Response {

    /**
     * URL has invalid parameters
     *
     * @param String|Exception $message
     *
     * @return Response
     */
    public function __construct($message = 'Resource Updated')
    {
        parent::__construct(['success' => $this->getMessage($message)], parent::HTTP_CREATED);
    }
}
