<?php

namespace Zagno\Crud\Traits;

use Zagno\Crud\Exception\ResourceBadRequestException;
use Zagno\Crud\Exception\ResourceNotFoundException;
use Zagno\Crud\Exception\ResourceFilterException;
use Way\Database\Model;

/**
 * @author Clive Zagno <clivez@gmail.com>
 *
 * Additional filtering for a model
 */
trait ModelFilterTrait {

    /**
     * Build a query tha Fetches a model and its relations from the object graph
     * must be called before model->find().
     *
     * @param Model $model
     * @param int $id
     * @param string $relation
     *
     * @return Model
     */
    protected function buildGraph($model, $id, $relations = '')
    {
        $relations = preg_replace('-graph\/?-','', $relations);
        $cacheKey  = [];

        if (empty($relations)) {
             return new ResourceFilterException("No relationships where specified");
        }

        $relations = explode(',', $relations);
        $cacheTags = array_merge([$this->cacheTag()], $this->cacheTagGraph($relations));
        $cacheKey  = array_merge(['graph'], $relations);

        if ( ! is_null($this->filters)) {
            $cacheKey = array_merge($cacheKey, $this->filters);
        }

        if ( ! is_null($id)) {
            $cacheKey = array_merge(['graph', $id], $relations);
        }

        return //$this//->applyFiltering($model)
                     $model->with($relations);
    }

     /**
     * Build a query that fetchs a model's relation.
     * must be called after model->find().
     *
     * @param Model $model
     * @param int $id
     * @param string $relation
     *
     * @return Model
     */
    public function buildRelation($model, $id, $relation)
    {
        $method = (string) $relation ;

        if ( ! $model) {
                throw new ResourceNotFoundException();
        }

        if (strstr($relation, '/')) {
            list($method, $relationId) = explode('/', $relation);
        }

        if ( ! method_exists($model, $method)) {
            throw new ResourceBadRequestException("Relation $method does not exist for resource");
        }

        if( ! isset($relationId)) {
            $model->setFilters($this->filters); // only filter for a collection
            $model->setSorting($this->sorting);

            return $model->$method()
                         ->get();
        }

        $model = $model->$method($relationId)

                       ->first();

        return $model;
    }

    public function prepareSortingArray($sortString)
    {
        $sortsOriginal = explode(',', urldecode($sortString));

        foreach ($sortsOriginal as $sortOriginal) {
            $sortOriginal = str_replace(' ', '', $sortOriginal);
            $order = 'asc';

            if (strstr($sortOriginal, '-')) {
                $order = 'desc';
                $sortOriginal = str_replace('-', '', $sortOriginal);
            }

            $sorts[$sortOriginal] = $order;
        }

        return $sorts;
    }

}
