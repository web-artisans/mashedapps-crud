<?php

namespace Zagno\Crud\Traits;

/**
 * @author Clive Zagno <clivez@gmail.com>
 *
 * A bunch of helper functions for cache key/tag names
 */
trait CacheTrait {

    /**
     * Works out a cache tag for the current class
     *
     * @return string
     */
    protected function cacheTag()
    {
        $class = explode('\\', get_class($this));
        $tag   = $class[count($class) - 1];
        $tag   = str_ireplace(array(',', 'controller'), array('_', '') , $tag);
        $tag   = rtrim($tag, "s");

        return strtolower($tag);
    }

    /**
     * Creates a cache tag using the object graph syntax for 3rd level relationship
     *
     * @param  array  $relations
     *
     * @return array
     */
    protected function cacheTagGraph(array $relations)
    {
       $newTags = array();

       $relations = array_map(
            function($key) use ( & $newTags) {
                $tags = explode('.', $key);

                if(count($tags) == 1) {
                    return rtrim($key, "s");
                }

                $newTags[$tags[1]] = rtrim($tags[1], "s");

                return rtrim($tags[0], "s");
            },
            $relations
        );

        return $relations + $newTags;
    }

    /**
     * Computes a cache key
     *
     * @param string|array $key
     *
     * @return string
     */
    protected function cacheKey($key)
    {
        if (is_array($key)) {
            $key = $this->cacheFormatArray($key);
        }

        $class = explode('\\', get_class($this));
        $key   = sprintf('%s_%s', $class[count($class) - 1] , $key);
        $key   = str_ireplace(array(',','controller'), array('_', '') , $key);

        return strtolower($key);
    }

    /**
     * Formats an array for use as a cache key
     *
     * @param array $array
     *
     * @return string
     */
    function cacheFormatArray(array $array) {
        $something = array_map (
            function ($value, $key) {
                if (is_numeric($key)) {
                    return $value;
                }

                return sprintf("%s_%s", $key, $value);
            },
            $array,
            array_keys($array)
        );

        return implode('_', $something);
    }

     /**
     * Computes a cache key form the URI
     *
     * @param string|array $key
     *
     * @return string
     */
    protected function cacheKeyFromUri()
    {
        $uri =  $_SERVER['QUERY_STRING'];;
        $uri = str_replace('_url=/', '', $uri);
        $uri = str_replace([',','/','?','&','='], '_', $uri);

        return $uri;

    }
}
