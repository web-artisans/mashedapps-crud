<?php

namespace Zagno\Crud\Traits;

use Zagno\Crud\Exception\ResourceBadRequestException;
use Zagno\Crud\Exception\ResourceNotFoundException;
use Zagno\Crud\Exception\ResourceFilterException;
use Way\Database\Model;

/**
 * @author Clive Zagno <clivez@gmail.com>
 *
 * Additional updates for a model
 */
trait ModelModificationTrait {

    /**
     * Creates a relations for a model
     *
     * @param  Model $model
     * @param  string $relation [description]
     *
     * @return null
     */
    protected function createRelation(Model $model, $relation, $relationId = null)
    {
        if ( ! isset($relationId)) {
            //new relation plus association
        }

        $model->$relation()->attach($relationId);

        return;
    }

    protected function deleteRelation(Model $model, $relation, $relationId = null)
    {
        if ( ! isset($relationId)) {
            $model->$relation()->detach();

            return;
        }

        $model->$relation()->detach($relationId);

        return;
    }
}
