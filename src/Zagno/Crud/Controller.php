<?php

namespace Zagno\Crud;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;
use Zagno\Crud\Exception\ResourceBadRequestException;
use Zagno\Crud\Exception\ResourceNotFoundException;
use Zagno\Crud\Exception\ResourceAccessException;
use Zagno\Crud\Exception\ResourceFilterException;
use Zagno\Crud\Response\ResourceNotFoundResponse;
use Zagno\Crud\Response\BadRequestResponse;
use Zagno\Crud\Response\ResourceCreatedResponse;
use Zagno\Crud\Response\ResourceUpdatedResponse;
use Zagno\Crud\Response\ResourceDeletedResponse;
use Zagno\Crud\Traits\CacheTrait;
use Zagno\Crud\Traits\ModelFilterTrait;
use Zagno\Crud\Traits\ModelModificationTrait;

/**
 * @author Clive Zagno <clivez@gmail.com>
 */
class Controller extends BaseController
{
    use CacheTrait, ModelFilterTrait, ModelModificationTrait;

    /**
     * @var Model
     */
    protected $model = null;

    /**
     * @var array
     */
    protected $filters = null;

    /**
     * @var array
     */
    protected $sorting = null;

    /**
     * @var array
     */
    protected $defaultFilters = ['per_page', 'page'];

    /**
     * @var integer
     */
    protected $cacheTime = 1;

    /**
     * constructor
     */
    public function __construct()
    {
        $this->filters = Input::all();

        // Input::all has a _url item
        if ( ! is_null($this->filters)) {
            unset($this->filters['_url']);
        }

        if (array_key_exists('sort', $this->filters))
        {
             $this->sorting = $this->prepareSortingArray($this->filters['sort']);
             unset($this->filters['sort']);
        }

        if(empty($this->filters)) {
            $this->filters = null;
        }
    }

    /**
     * Show of list of entities
     *
     * @return Response
     */
    public function index($relation = null)
    {
        $model = $this->model;
        $cacheKey = $this->cacheKeyFromUri();
        $uri      = \Request::path();

        $model->setFilters($this->filters);
        $model->setSorting($this->sorting);
        $model = $model->applyFiltersAndSorting();

        if ( ! is_null($relation) && stristr($uri, 'graph')) {
            $model = $this->buildGraph($model, null, $relation);
        }

        return $model//->cacheTags($this->cacheTag())
                     //->remember($this->cacheTime, $this->cacheKeyFromUri())
                     ->get();
    }

    /**
     * Show a specified entity or list of entities
     *
     * @param integer $id
     * @param string $relation
     *
     * @return Response
     */
    public function show($id, $relation = null)
    {
        $model    = $this->model;
        $cacheKey = $this->cacheKeyFromUri();

        if ( ! is_null($relation) && stristr($relation, 'graph')) {
            $model = $this->buildGraph($model, $id, $relation);
        }

        if ($this->isIdList($id)) {
            $id  = explode(',', $id);
        }

        $model = $model//->cacheTags($this->cacheTag())
                       //->remember($this->cacheTime, $cacheKey)
                       ->find($id);

        if ( ! is_null($relation)  && ! stristr($relation, 'graph')) {
                $model = $this->buildRelation($model, $id, $relation);

                return $model;
        }

        return $model;
    }

    /**
     * Creates a new entity
     *
     * @return Response
     */
    public function store()
    {
        $data = \Input::all();

        $model = new $this->model($data);

        if ( ! $model->save($data)) {
            throw new ResourceBadRequestException($model->getErrors()->first());
        }

        \Cache::forget($this->cacheKey('all'));

         $responseData = ['id' => $model->id];

        return new ResourceCreatedResponse("Created", $responseData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update($id, $relation = null, $relationId = null)
    {
        $model = $this->model->where('id', $id)->first();

        if ( ! $model) {
             throw new ResourceNotFoundException();
        }

        if ( ! is_null($relation)) {
           $this->createRelation($model, $relation, $relationId);

           \Cache::tags($this->cacheTag())->flush();

           return new ResourceUpdatedResponse();
        }

        $model->fill(\Input::all());

        if ( ! $model->save()) {
            throw new ResourceBadRequestException($model->getErrors()->first());
        }

        \Cache::tags($this->cacheTag())->flush();

        return new ResourceUpdatedResponse();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id, $relation = null, $relationId = null)
    {
        $model = $this->model->find($id);

        if ( ! $model) {
            throw new ResourceNotFoundException();
        }

        if ( ! is_null($relation)) {
            $this->deleteRelation($model, $relation, $relationId);
        }

        if (is_null($relation)) {
             $model->delete();
        }

        //\Cache::tags($this->cacheTag())->flush();

        return new ResourceDeletedResponse();
    }

    /**
     * Checks if the id passed comma seperated list or not
     *
     * @param string $id
     *
     * @return boolean
     */
    protected function isIdList($id)
    {
        $ids = explode(',', $id);

        if (count($ids) <= 1) {
            return false;
        }

        if (empty($ids[1])) {
            return false;
        }

        return true;
    }
}
