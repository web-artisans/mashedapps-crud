<?php

namespace Zagno\Crud\Exception;

/**
 * @author Clive Zagno <clivez@gmail.com>
 */
class ResourceBadRequestException extends \Exception
{
     protected $message = 'Bad Request';
}
