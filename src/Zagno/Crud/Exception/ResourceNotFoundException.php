<?php

namespace Zagno\Crud\Exception;

/**
 * @author Clive Zagno <clivez@gmail.com>
 */
class ResourceNotFoundException extends \Exception
{
     protected $message = 'Not Found';
}
