<?php

namespace Zagno\Crud\Exception;

/**
 * @author Simon Van Blerk <simonandrew@gmail.com>
 */
class ResourceAccessException extends \Exception
{
     protected $message = 'Access Denied';
}
