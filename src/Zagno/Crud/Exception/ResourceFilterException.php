<?php

namespace Zagno\Crud\Exception;

/**
 * @author Clive Zagno <clivez@gmail.com>
 */
class ResourceFilterException extends \Exception
{
     protected $message = 'Filter parameters are incorrect';
}
