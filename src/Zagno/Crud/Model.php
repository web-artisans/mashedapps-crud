<?php

namespace Zagno\Crud;

use Illuminate\Validation\Validator;
use Way\Database\Model as WayModel;

/**
 * @author Clive Zagno <clivez@gmail.com>
 *
 */
Class Model extends WayModel
{
    /**
     * @var Array
     */
    protected $filters = null;

    /**
     * @var Array
     */
    protected $sorting = null;

    /**
     * @var array
     */
    protected $defaultFilters = ['per_page', 'page'];

    /**
     * {@inheritdoc}
     */
    public function __construct(array $attributes = array(), Validator $validator = null)
    {
        parent::__construct($attributes, $validator);
    }

    /**
     * Runs default REST API filters
     *
     * @param  Model $model
     * @param  String $name
     * @param  String $value
     *
     * @return Model
     */
    public function applyDefaultFilter($model, $name, $value) {

        if($name == 'page' && ! isset($this->filters['per_page'])) {
              throw new Exception\ResourceFilterException("per_page required");
        }

        if($name == 'per_page' && ! isset($this->filters['page'])) {
              throw new Exception\ResourceFilterException("page required");
        }

        if($name == 'per_page') {
            $page = $this->filters['page'] - 1;
            $skip = ($page * $value);

            return $model->take($value)->skip($skip);
        }

        return $model;
    }

    /**
     * Filter a model
     *
     * @param Model|QueryBuilder $model
     *
     * @return Model|QueryBuilder
     */
    public function applyFilters($model = null)
    {
        if (is_null($model)) {
            $model = $this;
        }

        if (is_null($this->filters)) {
            return $model;
        }

        //$relation  = $model->getRelated();
        //$fillables = $relation->getFillable();

        foreach($this->filters as $filter => $value) {

            // if ( ! in_array($filter, $fillables)) {
            //     throw new ResourceFilterException('Can not filter on ' . $filter);
            // }

            if (in_array($filter, $this->defaultFilters)) {
                $model = $this->applyDefaultFilter($model, $filter, $value);

                continue;
            }

            if (strstr($value, '*')) {
                $value = str_replace('*', '%', $value);
                $model = $model->where($filter, 'like', $value);

                continue;
            }

            $model = $model->where($filter, $value);
        }

        return $model;
    }

    /**
     * Sort by
     *
     * @param Model|QueryBuilder $model
     *
     * @return Model|QueryBuilder
     */
    public function applySorting($model = null)
    {
        if (is_null($model)) {
            $model = $this;
        }

        if (is_null($this->sorting)) {
            return $model;
        }

        foreach($this->sorting as $column => $order) {
            $model = $model->orderBy($column, $order);
        }

        return $model;
    }

    /**
     * Convenience method to do filtering and sorting
     *
     * @param Model|QueryBuilder $model
     *
     * @return QueryBuilder
     */
    public function applyFiltersAndSorting($model = null)
    {
        if (is_null($model)) {
            $model = $this;
        }

        $model = $this->applyFilters($model);
        $model = $this->applySorting($model);

        return $model;
    }

    /**
     * Helper function to do a belongsToMany call with optional filters
     *
     * @param String $model
     * @param String $pivot_column
     * @param Int $id
     *
     * @return [type]               [description]
     */
    public function applyBelongsToMany($model, $pivot_column = null, $id = null)
    {
        if ( ! is_null($id)) {
            return $this->belongsToMany($model)->wherePivot($pivot_column,'=', $id);
        }

        return $this->applyFiltersAndSorting($this->belongsToMany($model));
    }

    /**
     * Helper function to do a hasMany call with optional filters
     *
     * @param String $model
     * @param Int $id
     *
     * @return Model
     */
    public function applyHasMany($model, $id = null)
    {
        if ( ! is_null($id)) {
            return $this->applyFilters($this->hasMany($model)->where('id', $id));
        }

        return $this->applyFiltersAndSorting($this->hasMany($model));
    }

    /**
     * Helper function to do a applyHasManyThrough call with filters
     *
     * @param String $model
     * @param Int $id
     *
     * @return Model
     */
    public function applyHasManyThrough($model, $throughModel)
    {
        return $this->applyFiltersAndSorting($this->hasManyThrough($model, $throughModel));
    }

    /**
     * setFilters
     *
     * @param Array $filters
     */
    public function setFilters($filters)
    {
        $this->filters = $filters;
    }

    /**
     * setSorting
     *
     * @param Array $filters
     */
    public function setSorting($sorting)
    {
        $this->sorting = $sorting;
    }
}
